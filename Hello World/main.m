//
//  main.m
//  Hello World
//
//  Created by Pranav Bhandari on 1/30/16.
//  Copyright (c) 2016 Pranav Bhandari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
