//
//  ViewController.h
//  Hello World
//
//  Created by Pranav Bhandari on 1/30/16.
//  Copyright (c) 2016 Pranav Bhandari. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end

