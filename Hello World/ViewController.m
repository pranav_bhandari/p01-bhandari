//
//  ViewController.m
//  Hello World
//
//  Created by Pranav Bhandari on 1/30/16.
//  Copyright (c) 2016 Pranav Bhandari. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSArray *colorArray;
    NSArray *countryArray;
    NSMutableArray *imgArray;
    
}
@end

@implementation ViewController
@synthesize picker,label,img;
- (void)viewDidLoad {

       colorArray = [[NSArray alloc]initWithObjects:@"Hello World",@"你好，世界",@"مرحبا بالعالم",@"हैलो वर्ल्ड",@"Hola Mundo",nil];
    countryArray = [[NSArray alloc]initWithObjects:@"English",@"Chinese",@"Arabic",@"Hindi",@"Spanish", nil];
    imgArray = [[NSMutableArray alloc]initWithObjects:@"usa.png",@"china.png",@"uae.png",@"india.png",@"spain.png", nil];
    
    [picker selectRow:2 inComponent:0 animated:YES];
    // Do any additional setup after loading the view, typically from a nib.
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 5;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [countryArray objectAtIndex:row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    label.text = [NSString stringWithFormat:@"%@",[colorArray objectAtIndex:[pickerView selectedRowInComponent:0]]];
    [img setImage:[UIImage imageNamed:[imgArray objectAtIndex:[pickerView selectedRowInComponent:0]]]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
