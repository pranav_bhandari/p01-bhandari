//
//  AppDelegate.h
//  Hello World
//
//  Created by Pranav Bhandari on 1/30/16.
//  Copyright (c) 2016 Pranav Bhandari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

